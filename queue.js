let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection.push(element); 
    return collection;
}

function dequeue() {
    let newCollection = [];

    for (let i = 1; i < collection.length; i++) {
        newCollection[i-1] = collection[i];
    }

    collection = newCollection;
    return collection;
}


function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};